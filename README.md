# Poetry Images

Poetry is a python package and dependency manager. This repository contains a registry of python base images intended for use within CI pipelines

By default these images use the alpine flavours of the python base images in order to reduce the overall size of the image
